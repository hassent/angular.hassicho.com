import { Component } from '@angular/core';
import { MaterialModule } from '@hassicho/core/components/material/material.module';
@Component({
  selector: 'app-root',
  template: `
  <mat-sidenav-container>
    <mat-sidenav #sidenav mode="side">hassicho</mat-sidenav>  
    <hass-header (navToggle)="sidenav.toggle()"></hass-header>
    <section class="hero is-fullheight is-warning is-bold">
      <div class="hero-body">
        <div class="container">
          <router-outlet></router-outlet>
        </div>
      </div>
    </section>
    <hass-footer></hass-footer>
  </mat-sidenav-container>
  `,
  styles: []
})
export class AppComponent {
  title = 'app';
}
