import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MaterialModule } from '@hassicho/core/components/material/material.module';
import { Router } from '@angular/router';
import { AuthorizationService } from '@hassicho/core/services/authorization.service';

@Component({
  selector: 'hass-header',
  template: `
  <nav class="navbar is-info" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" (click)="navOpen()">
      <img src="{{profile?.picture}}" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
    </a>
  </div>
  <div class="navbar-menu">
    <div class="navbar-end">
      <a routerLink="" class="navbar-item"><mat-icon> fingerprint </mat-icon> Battle</a>
      <a routerLink="create" class="navbar-item">🍨 Create</a>
      <a routerLink="leaderboard" class="navbar-item">⚡ Leaderboard</a>
      <a *ngIf="!auth.isAuthenticated()" (click)="auth.login()" class="navbar-item">Log In</a>
      <a *ngIf="auth.isAuthenticated()" (click)="auth.logout()" class="navbar-item">Log Out ({{ profile?.nickname }})</a>
    </div>
  </div>
  <button class="button navbar-burger">
  <span></span>
  <span></span>
  <span></span>
</button>
</nav>

  `,
  styles: []
})
export class HeaderComponent implements OnInit {
  profile:any;
  @Output() navToggle = new EventEmitter<boolean>();
  navOpen() {
    this.navToggle.emit(true);
  }
  constructor(private auth:AuthorizationService) {
    this.auth.handleAuthentication();
    this.getProfile();
   }

  ngOnInit() {
   
  }

  getProfile()
  {
    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
  }
  

}
