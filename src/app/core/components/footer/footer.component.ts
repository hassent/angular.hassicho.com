import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'hass-footer',
  template: `
  <footer class="footer">
  <p>
  <a href="https://bulma.io">
    <img src="/path/to/made-with-bulma.png" alt="Made with Bulma" width="128" height="24">
  </a>
  </p>
  
</footer>
  `,
  styles: []
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
