import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '@hassicho/core/components/header/header.component';
import { FooterComponent } from '@hassicho/core/components/footer/footer.component';
import { MaterialModule } from '@hassicho/core/components/material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MessengerService } from '@hassicho/core/services/messenger.service';
import { RouterModule } from '@angular/router';
import { AuthorizationService } from '@hassicho/core/services/authorization.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    RouterModule
  ],
  providers: [
    MessengerService,
    AuthorizationService
  ],
  declarations: [HeaderComponent, FooterComponent],
  exports: [HeaderComponent,FooterComponent]
})
export class CoreModule { }
