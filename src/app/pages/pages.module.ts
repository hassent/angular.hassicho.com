import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { CreateComponent } from './create/create.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [HomeComponent, CreateComponent, LeaderboardComponent],
  exports:[HomeComponent]
})
export class PagesModule { }
