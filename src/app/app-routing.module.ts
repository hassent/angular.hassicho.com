import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FooterComponent } from '@hassicho/core/components/footer/footer.component';
import { HeaderComponent } from '@hassicho/core/components/header/header.component';
import { HomeComponent } from '@hassicho/pages/home/home.component';
import { CreateComponent } from '@hassicho/pages/create/create.component';
import { LeaderboardComponent } from '@hassicho/pages/leaderboard/leaderboard.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'callback',redirectTo:''},
  {path:'create', component:CreateComponent},
  {path:'leaderboard', component:LeaderboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
